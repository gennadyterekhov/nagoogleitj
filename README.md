# intr_task_4
открытый вопрос лингвистики - в частности - орфографии
как представить слово "нагуглить" на латинице?
naguglitj
na-gugl-itj
nagugl-itj
nagugl'itj

na-google-tj
nagoogle-tj
nagoogle'tj
nagoogletj

na-google-itj
nagoogle-itj
nagoogle'itj
nagoogleitj

этот вопрос близок по духу вопросу "как представить сочетание "в гугле" на латинице?"
v gugle
v gugl-e
v gugl'e

v googl-e
v googl'e
v google

v google-e
v google'e
v googlee

лично мне более всего близки следующие варианты:
1. 
naguglitj
v gugle
как самые простые, с фонетической орфографией

2. na-google-itj
v google-e
как явно отделяющие русские морфемы от ненатурализованного (инородного) корня

3. nagoogleitj
v googlee
как неимеющие небуквенных знаков